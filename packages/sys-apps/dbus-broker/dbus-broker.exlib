# Copyright 2017 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SCM_SECONDARY_REPOSITORIES="
    c_dvar
    c_dvar_c_stdaux
    c_dvar_c_utf8
    c_dvar_c_utf8_c_stdaux
    c_ini
    c_ini_c_list
    c_ini_c_rbtree
    c_ini_c_rbtree_c_stdaux
    c_ini_c_stdaux
    c_ini_c_utf8
    c_ini_c_utf8_c_stdaux
    c_list
    c_rbtree
    c_rbtree_c_stdaux
    c_shquote
    c_shquote_c_stdaux
    c_stdaux
    c_utf8
    c_utf8_c_stdaux
"

SCM_c_dvar_REPOSITORY="https://github.com/c-util/c-dvar.git"
SCM_c_dvar_c_stdaux_REPOSITORY="https://github.com/c-util/c-stdaux.git"
SCM_c_dvar_c_utf8_REPOSITORY="https://github.com/c-util/c-utf8.git"
SCM_c_dvar_c_utf8_c_stdaux_REPOSITORY="https://github.com/c-util/c-stdaux.git"
SCM_c_ini_REPOSITORY="https://github.com/c-util/c-ini.git"
SCM_c_ini_c_list_REPOSITORY="https://github.com/c-util/c-list.git"
SCM_c_ini_c_rbtree_REPOSITORY="https://github.com/c-util/c-rbtree.git"
SCM_c_ini_c_rbtree_c_stdaux_REPOSITORY="https://github.com/c-util/c-stdaux.git"
SCM_c_ini_c_stdaux_REPOSITORY="https://github.com/c-util/c-stdaux.git"
SCM_c_ini_c_utf8_REPOSITORY="https://github.com/c-util/c-utf8.git"
SCM_c_ini_c_utf8_c_stdaux_REPOSITORY="https://github.com/c-util/c-stdaux.git"
SCM_c_list_REPOSITORY="https://github.com/c-util/c-list.git"
SCM_c_rbtree_REPOSITORY="https://github.com/c-util/c-rbtree.git"
SCM_c_rbtree_c_stdaux_REPOSITORY="https://github.com/c-util/c-stdaux.git"
SCM_c_shquote_REPOSITORY="https://github.com/c-util/c-shquote.git"
SCM_c_shquote_c_stdaux_REPOSITORY="https://github.com/c-util/c-stdaux.git"
SCM_c_stdaux_REPOSITORY="https://github.com/c-util/c-stdaux.git"
SCM_c_utf8_REPOSITORY="https://github.com/c-util/c-utf8.git"
SCM_c_utf8_c_stdaux_REPOSITORY="https://github.com/c-util/c-stdaux.git"

SCM_c_dvar_EXTERNAL_REFS="
    subprojects/c-stdaux:c_dvar_c_stdaux
    subprojects/c-utf8:c_dvar_c_utf8
"

SCM_c_dvar_c_utf8_EXTERNAL_REFS="
    subprojects/c-stdaux:c_dvar_c_utf8_c_stdaux
"

SCM_c_ini_EXTERNAL_REFS="
    subprojects/c-list:c_ini_c_list
    subprojects/c-rbtree:c_ini_c_rbtree
    subprojects/c-stdaux:c_ini_c_stdaux
    subprojects/c-utf8:c_ini_c_utf8
"

SCM_c_ini_c_rbtree_EXTERNAL_REFS="
    subprojects/c-stdaux:c_ini_c_rbtree_c_stdaux
"

SCM_c_ini_c_utf8_EXTERNAL_REFS="
    subprojects/c-stdaux:c_ini_c_utf8_c_stdaux
"

SCM_c_rbtree_EXTERNAL_REFS="
    subprojects/c-stdaux:c_rbtree_c_stdaux
"

SCM_c_shquote_EXTERNAL_REFS="
    subprojects/c-stdaux:c_shquote_c_stdaux
"

SCM_c_utf8_EXTERNAL_REFS="
    subprojects/c-stdaux:c_utf8_c_stdaux
"

SCM_EXTERNAL_REFS="
    subprojects/c-dvar:c_dvar
    subprojects/c-ini:c_ini
    subprojects/c-list:c_list
    subprojects/c-rbtree:c_rbtree
    subprojects/c-shquote:c_shquote
    subprojects/c-stdaux:c_stdaux
    subprojects/c-utf8:c_utf8
"

require github [ user=bus1 tag=v${PV} force_git_clone=true ] meson

SUMMARY="Linux D-Bus Message Broker"

LICENCES="Apache-2.0"
SLOT="0"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        sys-kernel/linux-headers[>=4.13]
        virtual/pkg-config
        doc? ( dev-python/docutils )
    build+run:
        dev-libs/expat
        sys-apps/systemd[>=230]
"

MESON_SRC_CONFIGURE_PARAMS=( -Daudit=false -Dselinux=false -Dlauncher=true -Dlinux-4-17=true -Dreference_test=false )
MESON_SRC_CONFIGURE_OPTION_SWITCHES=( 'doc docs' )

dbus-broker_src_test() {
    esandbox allow_net --bind "unix:"
    esandbox allow_net --bind "unix-abstract:*"
    esandbox allow_net --connect "unix-abstract:*"
    meson_src_test
    esandbox disallow_net --connect "unix-abstract:*"
    esandbox disallow_net --bind "unix-abstract:*"
    esandbox disallow_net --bind "unix:"
}

export_exlib_phases src_test
